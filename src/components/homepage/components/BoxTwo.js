import React from 'react'

function BoxTwo() {
    return (
        <div className="max-w-xs mr-2 bg-gray-100 border-2 rounded-lg shadow-lg lg:ml-0">
            <div className="px-4 py-4 text-4xl text-center">
                <i className="px-4 py-3 text-white bg-gray-700 border-2 rounded-full fa fa-database"></i>
            </div>
            <h1 className="text-2xl font-semibold text-center">Backend</h1>
            <div className="px-6 py-4 text-center">
                <span className="inline-block px-3 py-1 mr-2 text-sm font-semibold text-gray-700 bg-gray-400 rounded-full">
                    Laravel
                  </span>
                <span className="inline-block px-3 py-1 mr-2 text-sm font-semibold text-gray-700 bg-gray-400 rounded-full">
                    PHP
                  </span>
                <span className="inline-block px-3 py-1 mr-2 text-sm font-semibold text-gray-700 bg-gray-400 rounded-full">
                    Mysql
                  </span>
                <span className="inline-block px-3 py-1 mt-2 mr-2 text-sm font-semibold text-gray-700 bg-gray-400 rounded-full">
                    Mongo DB
                  </span>
                <span className="inline-block px-3 py-1 mt-2 mr-2 text-sm font-semibold text-gray-700 bg-gray-400 rounded-full">
                    JSON
                  </span>
                <span className="inline-block px-3 py-1 mt-2 mr-2 text-sm font-semibold text-gray-700 bg-gray-400 rounded-full">
                    Rest API
                  </span>
                <span className="inline-block px-3 py-1 mt-2 mr-2 text-sm font-semibold text-gray-700 bg-gray-400 rounded-full">
                    Node JS
                  </span>
                <span className="inline-block px-3 py-1 mt-2 mr-2 text-sm font-semibold text-gray-700 bg-gray-400 rounded-full">
                    AWS
                  </span>
            </div>
        </div>
    )
}

export default BoxTwo
